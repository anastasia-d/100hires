import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SidebarMenu {

    WebDriver driver;

    @FindBy(xpath = "//a[@href='/job/create']")
    WebElement jobsLink;

    public SidebarMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step
    public void clickJobsLink(){
        jobsLink.click();
    }
}
