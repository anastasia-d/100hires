import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

/**
 * @author anastasiya
 */

public class AddJobTest {

    private WebDriver driver;
    private String baseUrl;

    LoginPage loginPage = new LoginPage(driver);
    SidebarMenu sidebarMenu = new SidebarMenu(driver);

    @BeforeMethod(description = "Configure something before test")
    public void setUp() {
        File chromedriverExecutable = new File("drivers" + File.separator + "chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", chromedriverExecutable.getAbsolutePath());
        driver = new ChromeDriver();
        baseUrl = "https://app.100hires.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void simpleAddJob() {

        driver.get(baseUrl);
        loginPage.Login();
        sidebarMenu.clickJobsLink();

        /*driver.findElement(By.id("loginform-email")).sendKeys("a.dolzh@yandex.ru");
        driver.findElement(By.id("loginform-password")).sendKeys("11111111");

        driver.findElement(By.name("login-button")).click();*/


    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
    }

}


