import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;

    @FindBy(id = "loginform-email")
    WebElement emailInput;

    @FindBy(id = "loginform-password")
    WebElement password;

    @FindBy(name = "login-button")
    WebElement loginBtn;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step
    public void Login(){
        emailInput.sendKeys();
        password.sendKeys();
        loginBtn.click();
    }

}
